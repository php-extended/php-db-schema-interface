<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnStringInterface interface file.
 *
 * This interface defines a column with a string type.
 * 
 * @author Anastaszor
 */
interface ColumnStringInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeStringInterface
	 */
	public function getType() : TypeStringInterface;
	
	/**
	 * Gets the length of the column.
	 *
	 * @return integer
	 */
	public function getLength() : int;
	
	/**
	 * Gets the collation of the column.
	 *
	 * @return CollationInterface
	 */
	public function getCollation() : CollationInterface;
	
}
