<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableInterface interface file.
 * 
 * This interface specifies how variables are designed.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementVariableInterface extends StatementAssignableInterface
{
	
	/**
	 * Gets the name of the variable that is to be used.
	 * 
	 * @return string
	 */
	public function getVariableName() : string;
	
}
