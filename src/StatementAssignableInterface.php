<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementAssignableInterface interface file.
 * 
 * This interface is a generic interface for all statements that names a
 * column from a table, or from an abstract object.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementAssignableInterface extends StatementInterface
{
	
	// nothing to add
	
}
