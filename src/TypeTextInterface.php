<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeTextInterface interface file.
 *
 * This interface defines a column type which holds long text values.
 * 
 * @author Anastaszor
 */
interface TypeTextInterface extends TypeInterface
{
	
	/**
	 * Gets whether this String type is binary (true) or readable (false).
	 * 
	 * @return boolean
	 */
	public function isBinary() : bool;
	
	/**
	 * Gets the maximum length of the text type.
	 *
	 * @return integer
	 */
	public function getMaxLength() : int;
	
	/**
	 * Merges this text type with the other text type.
	 * 
	 * @param TypeTextInterface $type
	 * @return TypeTextInterface
	 */
	public function mergeWith(TypeTextInterface $type) : TypeTextInterface;
	
	/**
	 * Converts the given statement to the given type.
	 * 
	 * @param TypeTextInterface $type
	 * @param StatementValueTextInterface $statement
	 * @return StatementValueTextInterface
	 */
	public function castTo(TypeTextInterface $type, StatementValueTextInterface $statement) : StatementValueTextInterface;
	
}
