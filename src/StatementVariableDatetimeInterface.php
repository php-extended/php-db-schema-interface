<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableDatetimeInterface interface file.
 * 
 * This interface specifies how datetime variables are designed.
 * 
 * @author Anastaszor
 */
interface StatementVariableDatetimeInterface extends StatementValueDatetimeInterface, StatementVariableInterface
{
	
	// nothing to add
	
}
