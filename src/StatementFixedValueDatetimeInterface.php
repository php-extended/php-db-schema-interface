<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueDatetimeInterface interface file.
 * 
 * This interface specifies how fixed "hardcoded" datetime values are handled.
 * 
 * @author Anastaszor
 */
interface StatementFixedValueDatetimeInterface extends StatementFixedValueInterface, StatementValueDatetimeInterface
{
	
	// nothing to add
	
}
