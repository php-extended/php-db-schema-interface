<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use Iterator;

/**
 * DatabaseInterface interface file.
 * 
 * This interface defines a database schema.
 * 
 * @author Anastaszor
 */
interface DatabaseInterface
{
	
	/**
	 * Gets the name of the database.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the list of available schemas in that database.
	 * 
	 * @return Iterator<SchemaInterface>
	 */
	public function getSchemas() : Iterator;
	
	/**
	 * Gets the default collation of the database.
	 * 
	 * @return CollationInterface
	 */
	public function getDefaultCollation() : CollationInterface;
	
}
