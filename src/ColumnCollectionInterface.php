<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnCollectionInterface interface file.
 *
 * This interface defines a column with a collection type.
 * 
 * @author Anastaszor
 */
interface ColumnCollectionInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeCollectionInterface
	 */
	public function getType() : TypeCollectionInterface;
	
	/**
	 * Gets the actual values that are allowed in the collection.
	 *
	 * @return array<integer, string>
	 */
	public function getValues() : array;
	
}
