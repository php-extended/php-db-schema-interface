<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementAssignableInterface interface file.
 * 
 * This interface represents the different modifiers of column getting, such
 * as "NEW." or "OLD." 
 * 
 * @author Anastaszor
 */
interface StatementAccessModifierInterface
{
	
	/**
	 * Gets the modifier value.
	 * 
	 * @return string
	 */
	public function getModifier() : string;
	
}
