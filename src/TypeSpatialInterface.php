<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeNumberInterface interface file.
 *
 * This interface defines a column type which holds numerical value.
 * 
 * @author Anastaszor
 */
interface TypeSpatialInterface extends TypeInterface
{
	
	/**
	 * Gets the complexity of the figure. 1 for point, and +1 for each level
	 * of complexity.
	 * 
	 * @return integer
	 */
	public function getComplexity() : int;
	
	/**
	 * Merges with spatial type with the other spatial type.
	 * 
	 * @param TypeSpatialInterface $type
	 * @return TypeSpatialInterface
	 */
	public function mergeWith(TypeSpatialInterface $type) : TypeSpatialInterface;
	
	/**
	 * Converts the given statement to the given type.
	 * 
	 * @param TypeSpatialInterface $type
	 * @param StatementValueSpatialInterface $statement
	 * @return StatementValueSpatialInterface
	 */
	public function castTo(TypeSpatialInterface $type, StatementValueSpatialInterface $statement) : StatementValueSpatialInterface;
	
}
