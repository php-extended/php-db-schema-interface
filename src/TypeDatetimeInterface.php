<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeDatetimeInterface interface file.
 *
 * This interface defines a column type which holds temporal value.
 * 
 * @author Anastaszor
 */
interface TypeDatetimeInterface extends TypeInterface
{
	
	/**
	 * Gets whether this Datetime type has a date component.
	 * 
	 * @return boolean
	 */
	public function hasDate() : bool;
	
	/**
	 * Gets whether this Datetime type has a time component.
	 * 
	 * @return boolean
	 */
	public function hasTime() : bool;
	
	/**
	 * Gets the format of the Datetime format. This returns a string which
	 * correspond to the \date() function format argument.
	 *
	 * @return string
	 */
	public function getFormat() : string;
	
	/**
	 * Merges this datetime type with the other datetime type.
	 * 
	 * @param TypeDatetimeInterface $type
	 * @return TypeDatetimeInterface
	 */
	public function mergeWith(TypeDatetimeInterface $type) : TypeDatetimeInterface;
	
	/**
	 * Converts the given statement to the given type.
	 * 
	 * @param TypeDatetimeInterface $type
	 * @param StatementValueDatetimeInterface $statement
	 * @return StatementValueDatetimeInterface
	 */
	public function castTo(TypeDatetimeInterface $type, StatementValueDatetimeInterface $statement) : StatementValueDatetimeInterface;
	
}
