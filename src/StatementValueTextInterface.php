<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementValueTextInterface interface file.
 * 
 * This interface represents a components that returns a text value.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementValueTextInterface extends StatementValueInterface
{
	
	/**
	 * Gets the type for this value.
	 * 
	 * @return TypeTextInterface
	 */
	public function getType() : TypeTextInterface;
	
}
