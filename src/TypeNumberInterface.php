<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeNumberInterface interface file.
 *
 * This interface defines a column type which holds numerical value.
 * 
 * @author Anastaszor
 */
interface TypeNumberInterface extends TypeInterface
{
	
	/**
	 * Gets the maximum length of the number type.
	 *
	 * @return integer
	 */
	public function getMaximumLength() : int;
	
	/**
	 * Gets the maximum precision of the number type.
	 *
	 * @return ?integer
	 */
	public function getMaximumPrecision() : ?int;
	
	/**
	 * Merges this number type with the other number type.
	 * 
	 * @param TypeNumberInterface $type
	 * @return TypeNumberInterface
	 */
	public function mergeWith(TypeNumberInterface $type) : TypeNumberInterface;
	
	/**
	 * Converts the given statement to the given type.
	 * 
	 * @param TypeNumberInterface $type
	 * @param StatementValueNumberInterface $statement
	 * @return StatementValueNumberInterface
	 */
	public function castTo(TypeNumberInterface $type, StatementValueNumberInterface $statement) : StatementValueNumberInterface;
	
}
