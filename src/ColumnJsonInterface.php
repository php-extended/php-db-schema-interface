<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnJsonInterface interface file.
 *
 * This interface defines a column with a json type.
 * 
 * @author Anastaszor
 */
interface ColumnJsonInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeJsonInterface
	 */
	public function getType() : TypeJsonInterface;
	
}
