<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementValueNumberInterface interface file.
 * 
 * This interface represents a components that returns a number value.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementValueNumberInterface extends StatementValueInterface
{
	
	/**
	 * Gets the type for this value.
	 * 
	 * @return TypeNumberInterface
	 */
	public function getType() : TypeNumberInterface;
	
}
