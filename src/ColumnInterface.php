<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnInterface interface file.
 *
 * This interface defines a column in a table.
 * 
 * @author Anastaszor
 */
interface ColumnInterface
{
	
	/**
	 * Gets the name of the column.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this column may contain null values.
	 *
	 * @return boolean
	 */
	public function isNullAllowed() : bool;
	
	/**
	 * Gets the comment for this column.
	 *
	 * @return string
	 */
	public function getComment() : string;
	
	/**
	 * Visits this column with the given visitor.
	 * 
	 * @param ColumnVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor);
	
}
