<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeJsonInterface interface file.
 *
 * This interface defines a column type which holds json data.
 * 
 * @author Anastaszor
 */
interface TypeJsonInterface extends TypeInterface
{
	
	/**
	 * Gets the maximum depths of the type.
	 * 
	 * @return integer
	 */
	public function getMaxDepths() : int;
	
	/**
	 * Merges this json type with the other json type.
	 * 
	 * @param TypeJsonInterface $type
	 * @return TypeJsonInterface
	 */
	public function mergeWith(TypeJsonInterface $type) : TypeJsonInterface;
	
	/**
	 * Converts the given statement to the given type.
	 * 
	 * @param TypeJsonInterface $type
	 * @param StatementValueJsonInterface $statement
	 * @return StatementValueJsonInterface
	 */
	public function castTo(TypeJsonInterface $type, StatementValueJsonInterface $statement) : StatementValueJsonInterface;
	
}
