<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueNumberInterface interface file.
 * 
 * This interface specifies how fixed "hardcoded" number values are handled.
 * 
 * @author Anastaszor
 */
interface StatementFixedValueNumberInterface extends StatementFixedValueInterface, StatementValueNumberInterface
{
	
	// nothing to add
	
}
