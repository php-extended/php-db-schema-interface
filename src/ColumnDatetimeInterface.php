<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnDatetimeInterface interface file.
 *
 * This interface defines a column with a temporal type.
 * 
 * @author Anastaszor
 */
interface ColumnDatetimeInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeDatetimeInterface
	 */
	public function getType() : TypeDatetimeInterface;
	
	/**
	 * Gets whether the column auto-updates itself on update events.
	 *
	 * @return boolean
	 */
	public function hasAutoupdate() : bool;
	
}
