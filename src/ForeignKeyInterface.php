<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use Iterator;

/**
 * ForeignKeyInterface interface file.
 * 
 * This interface defines a foreign key, which is a link between the source
 * table and the target table, which may be used on one or more pair of columns.
 * 
 * @author Anastaszor
 */
interface ForeignKeyInterface
{
	
	/**
	 * Gets the name of the foreign key.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the name of the target table.
	 * 
	 * @return string
	 */
	public function getTargetTableName() : string;
	
	/**
	 * Gets the foreign key relations.
	 * 
	 * @return Iterator<ForeignKeyRelationInterface>
	 */
	public function getForeignKeyRelations() : Iterator;
	
	/**
	 * Gets the action which has to be done when the relation is updated.
	 * 
	 * @return ForeignKeyActionInterface
	 */
	public function getOnUpdateAction() : ForeignKeyActionInterface;
	
	/**
	 * Gets the action which has to be done when the relation is destroyed.
	 * 
	 * @return ForeignKeyActionInterface
	 */
	public function getOnDeleteAction() : ForeignKeyActionInterface;
	
}
