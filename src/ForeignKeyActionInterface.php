<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ForeignKeyActionInterface interface file.
 * 
 * This interface defines the actions which are done when the relation is
 * threatened.
 * 
 * @author Anastaszor
 */
interface ForeignKeyActionInterface
{
	
	/**
	 * Gets the name of the action.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
