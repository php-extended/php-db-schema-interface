<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeVisitorInterface interface file.
 * 
 * This interface is made to dispatch the visits amongst the different classes
 * of types.
 * 
 * @author Anastaszor
 */
interface TypeVisitorInterface
{
	
	/**
	 * Visits a collection type.
	 * 
	 * @param TypeCollectionInterface $typeCollection
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeCollection(TypeCollectionInterface $typeCollection);
	
	/**
	 * Visits a datetime type.
	 * 
	 * @param TypeDatetimeInterface $typeDatetime
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeDatetime(TypeDatetimeInterface $typeDatetime);
	
	/**
	 * Visits a json type.
	 * 
	 * @param TypeJsonInterface $typeJson
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeJson(TypeJsonInterface $typeJson);
	
	/**
	 * Visits a number type.
	 * 
	 * @param TypeNumberInterface $typeNumber
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeNumber(TypeNumberInterface $typeNumber);
	
	/**
	 * Visits a spatial type.
	 * 
	 * @param TypeSpatialInterface $typeSpatial
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeSpatial(TypeSpatialInterface $typeSpatial);
	
	/**
	 * Visits a string type.
	 * 
	 * @param TypeStringInterface $typeString
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeString(TypeStringInterface $typeString);
	
	/**
	 * Visits a text type.
	 * 
	 * @param TypeTextInterface $typeText
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitTypeText(TypeTextInterface $typeText);
	
}
