<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementDeclarationInterface interface file.
 * 
 * This interface represents a variable typing and declaration.
 * 
 * @author Anastaszor
 */
interface StatementDeclarationInterface extends StatementInterface
{
	
	/**
	 * Gets the name of the variable that is declared.
	 * 
	 * @return StatementVariableInterface
	 */
	public function getVariableName() : StatementVariableInterface;
	
	/**
	 * Gets the type of the variable that is declared.
	 * 
	 * @return TypeInterface
	 */
	public function getVariableType() : TypeInterface;
	
}
