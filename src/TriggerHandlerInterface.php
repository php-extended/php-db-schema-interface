<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TriggerHandlerInterface interface file.
 * 
 * This interface represents a handler of a trigger, which may be used at
 * different times when the trigger was fired.
 * 
 * @author Anastaszor
 */
interface TriggerHandlerInterface
{
	
	/**
	 * Gets the handler name of this handler.
	 * 
	 * @return string
	 */
	public function getHandlerName() : string;
	
}
