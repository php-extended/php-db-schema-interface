<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnVisitorInterface interface file.
 * 
 * This interface is made to dispatch the visits amongst the different classes
 * of columns.
 * 
 * @author Anastaszor
 */
interface ColumnVisitorInterface
{
	
	/**
	 * Visits a collection column.
	 * 
	 * @param ColumnCollectionInterface $columnCollection
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnCollection(ColumnCollectionInterface $columnCollection);
	
	/**
	 * Visits a datetime column.
	 * 
	 * @param ColumnDatetimeInterface $columnDatetime
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnDatetime(ColumnDatetimeInterface $columnDatetime);
	
	/**
	 * Visits a json column.
	 * 
	 * @param ColumnJsonInterface $columnJson
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnJson(ColumnJsonInterface $columnJson);
	
	/**
	 * Visits a number column.
	 * 
	 * @param ColumnNumberInterface $columnNumber
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnNumber(ColumnNumberInterface $columnNumber);
	
	/**
	 * Visits a spatial column.
	 * 
	 * @param ColumnSpatialInterface $columnSpatial
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnSpatial(ColumnSpatialInterface $columnSpatial);
	
	/**
	 * Visits a string column.
	 * 
	 * @param ColumnStringInterface $columnString
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnString(ColumnStringInterface $columnString);
	
	/**
	 * Visits a text column.
	 * 
	 * @param ColumnTextInterface $columnText
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnText(ColumnTextInterface $columnText);
	
}
