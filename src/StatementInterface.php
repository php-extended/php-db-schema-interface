<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementInterface interface file.
 * 
 * This interface is a generic interface for all statements that may occur.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementInterface
{
	
	/**
	 * Visits this statement with the given visitor.
	 * 
	 * @param StatementVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor);
	
}
