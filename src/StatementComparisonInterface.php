<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementComparisonInterface interface file.
 * 
 * This interface specifies how comparisons are made.
 * 
 * @author Anastaszor
 */
interface StatementComparisonInterface extends StatementInterface
{
	
	/**
	 * Gets the operator of the comparison.
	 * 
	 * @return string
	 * @todo reify
	 */
	public function getOperator() : string;
	
	/**
	 * Gets the left value of the comparison.
	 * 
	 * @return StatementValueInterface
	 */
	public function getLeftOperand() : StatementValueInterface;
	
	/**
	 * Gets the right value of the comparison.
	 * 
	 * @return StatementValueInterface
	 */
	public function getRightOperand() : StatementValueInterface;
	
}
