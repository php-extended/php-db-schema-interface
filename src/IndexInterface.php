<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * IndexInterface interface file.
 *
 * This interface defines an index.
 * 
 * @author Anastaszor
 */
interface IndexInterface
{
	
	/**
	 * Gets the name of this index.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this index has an unique constraint.
	 *
	 * @return boolean
	 */
	public function isUnique() : bool;
	
	/**
	 * Gets the names of the columns that are indexed.
	 *
	 * @return array<integer, string>
	 */
	public function getColumnNames() : array;
	
}
