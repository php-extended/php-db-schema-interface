<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnDatetimeInterface interface file.
 * 
 * This interface specifies how columns datetime values are handled.
 * 
 * @author Anastaszor
 */
interface StatementColumnDatetimeInterface extends StatementColumnInterface, StatementValueDatetimeInterface
{
	
	/**
	 * Gets the column used by this statement.
	 * 
	 * @return ColumnDatetimeInterface
	 */
	public function getColumn() : ColumnDatetimeInterface;
	
}
