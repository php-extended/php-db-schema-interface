<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use Iterator;

/**
 * SchemaInterface interface file.
 * 
 * This interface defines a schema.
 * 
 * @author Anastaszor
 */
interface SchemaInterface
{
	
	/**
	 * Gets the name of the schema.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the tables of the schema.
	 * 
	 * @return Iterator<TableInterface>
	 */
	public function getTables() : Iterator;
	
	/**
	 * Gets the default collation of the schema.
	 * 
	 * @return CollationInterface
	 */
	public function getDefaultCollation() : CollationInterface;
	
}
