<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use Iterator;

/**
 * TableInterface interface file.
 *
 * This interface defines a table schema.
 * 
 * @author Anastaszor
 */
interface TableInterface
{
	
	/**
	 * Gets the name of this table.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets all the columns of the table.
	 *
	 * @return Iterator<ColumnInterface>
	 */
	public function getColumns() : Iterator;
	
	/**
	 * Gets the primary key of this table, if any.
	 *
	 * @return PrimaryKeyInterface
	 */
	public function getPrimaryKey() : PrimaryKeyInterface;
	
	/**
	 * Gets the foreign keys of this table, if any.
	 * 
	 * @return Iterator<ForeignKeyInterface>
	 */
	public function getForeignKeys() : Iterator;
	
	/**
	 * Gets all the indexes of this table which are not the primary key.
	 *
	 * @return Iterator<IndexInterface>
	 */
	public function getIndexes() : Iterator;
	
	/**
	 * Gets all the triggers of this table, if any.
	 * 
	 * @return Iterator<TriggerInterface>
	 */
	public function getTriggers() : Iterator;
	
	/**
	 * Gets the default collation for this table.
	 *
	 * @return CollationInterface
	 */
	public function getDefaultCollation() : CollationInterface;
	
	/**
	 * Gets the engine that will use the table.
	 *
	 * @return EngineInterface
	 */
	public function getEngine() : EngineInterface;
	
	/**
	 * Gets the comment for this table.
	 *
	 * @return ?string
	 */
	public function getComment() : ?string;
	
}
