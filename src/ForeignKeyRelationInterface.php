<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ForeignKeyRelationInterface interface file.
 * 
 * This interface defines a link between the column in the source table and the
 * column in the target table.
 * 
 * @author Anastaszor
 */
interface ForeignKeyRelationInterface
{
	
	/**
	 * Gets the name of the source field in the source table.
	 * 
	 * @return string
	 */
	public function getSourceFieldName() : string;
	
	/**
	 * Gets the name of the target field in the target table.
	 * 
	 * @return string
	 */
	public function getTargetFieldName() : string;
	
}
