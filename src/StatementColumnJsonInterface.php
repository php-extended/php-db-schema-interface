<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnJsonInterface interface file.
 * 
 * This interface specifies how columns json values are handled.
 * 
 * @author Anastaszor
 */
interface StatementColumnJsonInterface extends StatementColumnInterface, StatementValueJsonInterface
{
	
	/**
	 * Gets the column used by this statement.
	 * 
	 * @return ColumnJsonInterface
	 */
	public function getColumn() : ColumnJsonInterface;
	
}
