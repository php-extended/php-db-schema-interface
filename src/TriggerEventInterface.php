<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TriggerEventInterface interface file.
 * 
 * This interface represents the events on which triggers may occur.
 * 
 * @author Anastaszor
 */
interface TriggerEventInterface
{
	
	/**
	 * Gets the event name of this trigger event.
	 * 
	 * @return string
	 */
	public function getEventName() : string;
	
}
