<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementValueInterface interface file.
 * 
 * This interface is a super interface for all statements that returns values.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementValueInterface extends StatementInterface
{
	
	// nothing to add
	
}
