<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnSpatialInterface interface file.
 *
 * This interface defines a column with a spatial type.
 * 
 * @author Anastaszor
 */
interface ColumnSpatialInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeSpatialInterface
	 */
	public function getType() : TypeSpatialInterface;
	
}
