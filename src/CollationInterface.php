<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use PhpExtended\Charset\CharacterSetInterface;

/**
 * CollationInterface interface file.
 *
 * This interface defines a collation.
 * 
 * @author Anastaszor
 */
interface CollationInterface
{
	
	/**
	 * Gets the name of this collation.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the character set for this collation.
	 *
	 * @return CharacterSetInterface
	 */
	public function getCharacterSet() : CharacterSetInterface;
	
	/**
	 * Gets whether this collation is case insensitive.
	 * 
	 * @return boolean
	 */
	public function isCaseInsensitive() : bool;
	
}
