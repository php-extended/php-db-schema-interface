<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementAssignationInterface interface file.
 * 
 * This interface specifies how assignations are made.
 * 
 * @author Anastaszor
 */
interface StatementAssignationInterface extends StatementInterface
{
	
	/**
	 * Gets the operator of the assignment.
	 * 
	 * @return string
	 * @todo reify
	 */
	public function getOperator() : string;
	
	/**
	 * Gets the variable that is to be assigned.
	 * 
	 * @return StatementAssignableInterface
	 */
	public function getVariable() : StatementAssignableInterface;
	
	/**
	 * Gets the value that is to be assigned.
	 * 
	 * @return StatementValueInterface
	 */
	public function getStatement() : StatementValueInterface;
	
}
