<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use Iterator;

/**
 * StatementFunctionCallInterface interface file.
 * 
 * This interface specifies how function calls are handled.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementFunctionCallInterface extends StatementInterface
{
	
	/**
	 * The name of the function to call.
	 * 
	 * @return string
	 */
	public function getFunctionName() : string;
	
	/**
	 * The parameters, in order, as argument for this call.
	 * 
	 * @return Iterator<StatementValueInterface>
	 */
	public function getParameters() : Iterator;
	
}
