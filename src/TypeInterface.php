<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeInterface interface file.
 *
 * This interface defines any column type.
 * 
 * @author Anastaszor
 */
interface TypeInterface
{
	
	/**
	 * The name of the type.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Visits this type with the given visitor.
	 * 
	 * @param TypeVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(TypeVisitorInterface $visitor);
	
}
