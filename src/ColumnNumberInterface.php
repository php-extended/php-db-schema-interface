<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnNumberInterface interface file.
 *
 * This interface defines a column with a numeric type.
 * 
 * @author Anastaszor
 */
interface ColumnNumberInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeNumberInterface
	 */
	public function getType() : TypeNumberInterface;
	
	/**
	 * Gets the length of the column.
	 *
	 * @return integer
	 */
	public function getLength() : int;
	
	/**
	 * Gets the precision of the column.
	 *
	 * @return ?integer
	 */
	public function getPrecision() : ?int;
	
	/**
	 * Gets whether the column stored the numbers as unsigned values.
	 *
	 * @return boolean
	 */
	public function isUnsigned() : bool;
	
	/**
	 * Gets whether this column has auto increment.
	 *
	 * @return boolean
	 */
	public function hasAutoIncrement() : bool;
	
}
