<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * DialectInterface class file.
 *
 * This class represents a dialect, i.e. a subversion of SQL that is parsable
 * and understandable by a specific RDBMS.
 * 
 * @author Anastaszor
 */
interface DialectInterface
{
	
	/**
	 * Gets the CREATE DATABASE sql order.
	 * 
	 * @param DatabaseInterface $database
	 * @return string
	 */
	public function createDatabase(DatabaseInterface $database) : string;
	
	/**
	 * Gets the DROP DATABASE sql order.
	 * 
	 * @param DatabaseInterface $database
	 * @return string
	 */
	public function dropDatabase(DatabaseInterface $database) : string;
	
	/**
	 * Gets the CREATE SCHEMA sql order.
	 * 
	 * @param SchemaInterface $schema
	 * @return string
	 */
	public function createSchema(SchemaInterface $schema) : string;
	
	/**
	 * Gets the DROP SCHEMA sql order.
	 * 
	 * @param SchemaInterface $schema
	 * @return string
	 */
	public function dropSchema(SchemaInterface $schema) : string;
	
	/**
	 * Gets the CREATE TABLE sql order.
	 *
	 * @param TableInterface $table
	 * @return string
	 */
	public function createTable(TableInterface $table) : string;
	
	/**
	 * Gets the DROP TABLE sql order.
	 * 
	 * @param TableInterface $table
	 * @return string
	 */
	public function dropTable(TableInterface $table) : string;
	
	/**
	 * Gets the TRUNCATE TABLE sql order.
	 * 
	 * @param TableInterface $table
	 * @return string
	 */
	public function truncateTable(TableInterface $table) : string;
	
	/**
	 * Gets the ALTER TABLE ADD CONSTRAINT FOREIGN KEY sql order that alters
	 * the source table to build the relation described by the foreign key to
	 * the target table.
	 * 
	 * @param TableInterface $sourceTable
	 * @param ForeignKeyInterface $foreignKey
	 * @return string
	 */
	public function alterTableAddForeignKey(TableInterface $sourceTable, ForeignKeyInterface $foreignKey) : string;
	
	/**
	 * Gets the ALTER TABLE DROP FOREIGN KEY sql order that alters the source
	 * table to remove the relation described by the foreign key to the target
	 * table.
	 * 
	 * @param TableInterface $sourceTable
	 * @param ForeignKeyInterface $foreignKey
	 * @return string
	 */
	public function alterTableDropForeignKey(TableInterface $sourceTable, ForeignKeyInterface $foreignKey) : string;
	
	/**
	 * Gets the CREATE TRIGGER sql order.
	 * 
	 * @param TableInterface $table
	 * @param TriggerInterface $trigger
	 * @return string
	 */
	public function createTrigger(TableInterface $table, TriggerInterface $trigger) : string;
	
	/**
	 * Gets the DROP TRIGGER sql order.
	 * 
	 * @param TableInterface $table
	 * @param TriggerInterface $trigger
	 * @return string
	 */
	public function dropTrigger(TableInterface $table, TriggerInterface $trigger) : string;
	
}
