<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * PrimaryKeyInterface interface file.
 *
 * This interface defines a primary key.
 * 
 * @author Anastaszor
 */
interface PrimaryKeyInterface
{
	
	/**
	 * Gets the names of the columns that are indexed.
	 *
	 * @return array<integer, string>
	 */
	public function getColumnNames() : array;
	
}
