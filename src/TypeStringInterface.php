<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeStringInterface interface file.
 *
 * This interface defines a column which holds indexable string values.
 * 
 * @author Anastaszor
 */
interface TypeStringInterface extends TypeInterface
{
	
	/**
	 * Gets whether this String type is binary (true) or readable (false).
	 * 
	 * @return boolean
	 */
	public function isBinary() : bool;
	
	/**
	 * Gets the maximum number of characters in the string.
	 *
	 * @return integer
	 */
	public function getMaxLength() : int;
	
	/**
	 * Merges this string type with the other string type.
	 * 
	 * @param TypeStringInterface $type
	 * @return TypeStringInterface
	 */
	public function mergeWith(TypeStringInterface $type) : TypeStringInterface;
	
	/**
	 * Converts the given statement to the given type.
	 * 
	 * @param TypeStringInterface $type
	 * @param StatementValueStringInterface $statement
	 * @return StatementValueStringInterface
	 */
	public function castTo(TypeStringInterface $type, StatementValueStringInterface $statement) : StatementValueStringInterface;
	
}
