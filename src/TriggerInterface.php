<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use Iterator;

/**
 * TriggerInterface interface file.
 * 
 * This interface specifies a trigger on a table.
 * 
 * @author Anastaszor
 */
interface TriggerInterface
{
	
	/**
	 * The name of the trigger.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the event on which this trigger will fire.
	 * 
	 * @return TriggerEventInterface
	 */
	public function getEvent() : TriggerEventInterface;
	
	/**
	 * Gets the handler that will guide this trigger.
	 * 
	 * @return TriggerHandlerInterface
	 */
	public function getHandler() : TriggerHandlerInterface;
	
	/**
	 * Gets the statements that will be executed when this trigger is fired.
	 * 
	 * @return Iterator<StatementInterface>
	 */
	public function getStatements() : Iterator;
	
}
