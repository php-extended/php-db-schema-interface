<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * EngineInterface interface file.
 *
 * This interface defines an engine.
 * 
 * @author Anastaszor
 */
interface EngineInterface
{
	
	/**
	 * Gets the name of the engine.
	 *
	 * @return string
	 */
	public function getName() : string;
	
}
