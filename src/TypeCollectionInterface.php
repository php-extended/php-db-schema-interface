<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * TypeCollectionInterface interface file.
 *
 * This  interface defines a column type which holds values among predefined
 * collections.
 * 
 * @author Anastaszor
 */
interface TypeCollectionInterface extends TypeInterface
{
	
	/**
	 * Gets the maximum number of elements the type can hold.
	 *
	 * @return integer
	 */
	public function getMaxElements() : int;
	
	/**
	 * Gets whether the column allows for multiple selection.
	 *
	 * @return boolean
	 */
	public function allowsMultiple() : bool;
	
	/**
	 * Merges this collection type with the other collection type.
	 * 
	 * @param TypeCollectionInterface $type
	 * @return TypeCollectionInterface
	 */
	public function mergeWith(TypeCollectionInterface $type) : TypeCollectionInterface;
	
}
