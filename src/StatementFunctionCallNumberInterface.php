<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFunctionCallNumberInterface interface file.
 * 
 * This interface specifies how function calls that returns numbers are handled.
 * 
 * @author Anastaszor
 */
interface StatementFunctionCallNumberInterface extends StatementFunctionCallInterface, StatementValueNumberInterface
{
	
	// nothing to add
	
}
