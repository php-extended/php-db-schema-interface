<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueInterface interface file.
 * 
 * This interface specifies how fixed "hardcoded" values are handled.
 * 
 * @author Anastaszor
 * @abstract
 */
interface StatementFixedValueInterface extends StatementInterface
{
	
	/**
	 * Gets the fixed value value, on string form.
	 * 
	 * @return string
	 */
	public function getFixedValue() : string;
	
}
