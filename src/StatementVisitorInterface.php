<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVisitorInterface interface file.
 * 
 * This interface specifies the objects to visit to a visitor.
 * 
 * @author Anastaszor
 * @todo add operators
 * @todo add casts
 */
interface StatementVisitorInterface
{
	
	/**
	 * Visits the statement assignation. This MAY return null if needed.
	 * 
	 * @param StatementAssignationInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStatementAssignation(StatementAssignationInterface $statement);
	
	/**
	 * Visits the statement comparaison. This MAY return null if needed.
	 * 
	 * @param StatementComparisonInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStatementComparison(StatementComparisonInterface $statement);
	
	/**
	 * Visits the statement declaration. This MAY return null if needed.
	 * 
	 * @param StatementDeclarationInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStatementDeclaration(StatementDeclarationInterface $statement);
	
	// ---- ---- ---- ---- Fixed Values Visits ---- ---- ---- ---- \\
	
	/**
	 * Visits the fixed value datetime statement. This MAY return null if
	 * needed.
	 * 
	 * @param StatementFixedValueDatetimeInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueDatetime(StatementFixedValueDatetimeInterface $statement);
	
	/**
	 * Visits the fixed value json statement. This MAY return null if needed.
	 * 
	 * @param StatementFixedValueJsonInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueJson(StatementFixedValueJsonInterface $statement);
	
	/**
	 * Visits the fixed value number statement. This MAY return null if needed.
	 * 
	 * @param StatementFixedValueNumberInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueNumber(StatementFixedValueNumberInterface $statement);
	
	/**
	 * Visits the fixed value spatial statement. This MAY return null if needed.
	 * 
	 * @param StatementFixedValueSpatialInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueSpatial(StatementFixedValueSpatialInterface $statement);
	
	/**
	 * Visits the fixed value string statement. This MAY return null if needed.
	 * 
	 * @param StatementFixedValueStringInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueString(StatementFixedValueStringInterface $statement);
	
	/**
	 * Visits the fixed value text statement. This MAY return null if needed.
	 * 
	 * @param StatementFixedValueTextInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueText(StatementFixedValueTextInterface $statement);
	
	// ---- ---- ---- ---- Variables Visits ---- ---- ---- ---- \\
	
	/**
	 * Visits the variable datetime statement. This MAY return null if needed.
	 * 
	 * @param StatementVariableDatetimeInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableDatetime(StatementVariableDatetimeInterface $statement);
	
	/**
	 * Visits the variable json statement. This MAY return null if needed.
	 * 
	 * @param StatementVariableJsonInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableJson(StatementVariableJsonInterface $statement);
	
	/**
	 * Visits the variable number statement. This MAY return null if needed.
	 * 
	 * @param StatementVariableNumberInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableNumber(StatementVariableNumberInterface $statement);
	
	/**
	 * Visits the variable spatial statement. This MAY return null if needed.
	 * 
	 * @param StatementVariableSpatialInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableSpatial(StatementVariableSpatialInterface $statement);
	
	/**
	 * Visits the variable string statement. This MAY return null if needed.
	 * 
	 * @param StatementVariableStringInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableString(StatementVariableStringInterface $statement);
	
	/**
	 * Visits the variable text statement. This MAY return null if needed.
	 * 
	 * @param StatementVariableTextInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableText(StatementVariableTextInterface $statement);
	
	// ---- ---- ---- ---- Function Calls Visits ---- ---- ---- ---- \\
	
	/**
	 * Visits the function call datetime statement. This MAY return null if
	 * needed.
	 * 
	 * @param StatementFunctionCallDatetimeInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallDatetime(StatementFunctionCallDatetimeInterface $statement);
	
	/**
	 * Visits the function call json statement. This MAY return null if needed.
	 * 
	 * @param StatementFunctionCallJsonInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallJson(StatementFunctionCallJsonInterface $statement);
	
	/**
	 * Visits the function call number statement. This MAY return null if needed.
	 * 
	 * @param StatementFunctionCallNumberInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallNumber(StatementFunctionCallNumberInterface $statement);
	
	/**
	 * Visits the function call spatial statement. This MAY return null if needed.
	 * 
	 * @param StatementFunctionCallSpatialInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallSpatial(StatementFunctionCallSpatialInterface $statement);
	
	/**
	 * Visits the function call string statement. This MAY return null if needed.
	 * 
	 * @param StatementFunctionCallStringInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallString(StatementFunctionCallStringInterface $statement);
	
	/**
	 * Visits the function call text statement. This MAY return null if needed.
	 * 
	 * @param StatementFunctionCallTextInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallText(StatementFunctionCallTextInterface $statement);
	
	// ---- ---- ---- ---- Column Visits ---- ---- ---- ---- \\
	
	/**
	 * Visits the column datetime statement. This MAY return null if needed.
	 * 
	 * @param StatementColumnDatetimeInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnDatetime(StatementColumnDatetimeInterface $statement);
	
	/**
	 * Visits the column json statement. This MAY return null if needed.
	 * 
	 * @param StatementColumnJsonInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnJson(StatementColumnJsonInterface $statement);
	
	/**
	 * Visits the column number statement. This MAY return null if needed.
	 * 
	 * @param StatementColumnNumberInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnNumber(StatementColumnNumberInterface $statement);
	
	/**
	 * Visits the column spatial statement. This MAY return null if needed.
	 * 
	 * @param StatementColumnSpatialInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnSpatial(StatementColumnSpatialInterface $statement);
	
	/**
	 * Visits the column string statement. This MAY return null if needed.
	 * 
	 * @param StatementColumnStringInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnString(StatementColumnStringInterface $statement);
	
	/**
	 * Visits the column text statement. This MAY return null if needed.
	 * 
	 * @param StatementColumnTextInterface $statement
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnText(StatementColumnTextInterface $statement);
	
}
