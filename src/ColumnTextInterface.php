<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnTextInterface interface file.
 *
 * This interface defines a column with a text type.
 * 
 * @author Anastaszor
 */
interface ColumnTextInterface extends ColumnInterface
{
	
	/**
	 * Gets the type of the column.
	 *
	 * @return TypeTextInterface
	 */
	public function getType() : TypeTextInterface;
	
	/**
	 * Gets the collation of the column.
	 *
	 * @return CollationInterface
	 */
	public function getCollation() : CollationInterface;
	
}
