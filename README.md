# php-extended/php-db-schema-interface
A library which provides interfaces to database schema abstractions.

![coverage](https://gitlab.com/php-extended/php-db-schema-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-db-schema-interface ^9`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-db-schema-object`](https://gitlab.com/php-extended/php-db-schema-object).


## License

MIT (See [license file](LICENSE)).
